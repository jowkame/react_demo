import { StackNavigator } from 'react-navigation';
import { enhance } from 'react-navigation-addons';
import ContentScreen from './src/Screens/Content/ContentScreen';
import EditScreen from './src/Screens/Edit/EditScreen';

export default enhance(StackNavigator)({
  Home: { screen: ContentScreen },
  Edit: { screen: EditScreen },
});
