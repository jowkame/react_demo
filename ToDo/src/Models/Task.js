class Task {
  constructor(title, details) {
    this.title = title;
    this.details = details;
    this.createdAt = new Date();
  }
}

module.exports = Task;
