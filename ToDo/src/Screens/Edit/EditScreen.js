import React from 'react';
import { Text, StyleSheet, TextInput, View } from 'react-native';
import StorageHelper from '../../Utils/StorageHelper';

class EditScreen extends React.Component {
    static navigationOptions = {
      title: 'Add task',
      headerTintColor: '#576574',
    };

    state = {
      title: '',
      details: '',
    };

    _next = () => this._detailsInput && this._detailsInput.focus();

    _submit = () => {
      const storageHelper = new StorageHelper();
      storageHelper.saveTask(this.state.title, this.state.details, this._taskDidSave);
    };

    _taskDidSave = () => {
      this.props.navigation.state.params.updateItems();
      this.props.navigation.goBack();
    };

    render() {
      return (
        <View style={styles.container}>
          <View style={styles.header}>
            <Text style={styles.description}>
                    Please, enter some data:
            </Text>
          </View>
          <TextInput
            style={styles.input}
            value={this.state.name}
            onChangeText={title => this.setState({ title })}
            ref={(ref) => { this._titleInput = ref; }}
            placeholder="Title"
            autoFocus
            autoCapitalize="words"
            autoCorrect
            keyboardType="default"
            returnKeyType="next"
            onSubmitEditing={this._next}
            blurOnSubmit={false}
          />
          <TextInput
            style={styles.input}
            value={this.state.email}
            onChangeText={details => this.setState({ details })}
            ref={(ref) => { this._detailsInput = ref; }}
            placeholder="Details"
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="default"
            returnKeyType="send"
            onSubmitEditing={this._submit}
            blurOnSubmit
          />
        </View>
      );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
  },
  header: {
    paddingTop: 20,
    padding: 20,
    backgroundColor: '#336699',
  },
  description: {
    fontSize: 14,
    color: 'white',
  },
  input: {
    margin: 20,
    marginBottom: 0,
    height: 34,
    paddingHorizontal: 10,
    borderRadius: 4,
    borderColor: '#ccc',
    borderWidth: 1,
    fontSize: 16,
  },
});

export default EditScreen;
