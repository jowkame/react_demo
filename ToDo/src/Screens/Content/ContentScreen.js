import React from 'react';
import { StyleSheet, TouchableOpacity, Text, View } from 'react-native';
import SortableListView from 'react-native-sortable-listview';
import RowComponent from './RowComponent';
import StorageHelper from '../../Utils/StorageHelper';

let data = {};
let order = [];

class ContentScreen extends React.Component {
  componentWillMount() {
    this.updateItems();

    this.props.navigation.setOptions({
      headerTitle: 'Tasks',
      headerRight: (
        <TouchableOpacity style={styles.buttonStyle} onPress={this.openEditScreen}>
          <Text style={styles.buttonText}>Add</Text>
        </TouchableOpacity>
      ),
    });
  }

  openEditScreen = () => {
    this.props.navigation.navigate('Edit', {
      updateItems: this.updateItems,
    });
  }

  updateItems = () => {
    const storageHelper = new StorageHelper();
    storageHelper.fetchTasks((tasks) => {
      const dict = {};

      tasks.forEach((task) => {
        const subDict = { text: task.title };
        const key = String(task.details);
        dict[key] = subDict;
      });

      data = dict;
      order = Object.keys(data);

      this.forceUpdate();
    });
  }

  placeholderView() {
    return (
      <View style={styles.container}>
        <Text style={styles.placeholderText}>Please, add some tasks...</Text>
      </View>
    );
  }

  contentTableView() {
    return (
      <SortableListView
        style={{ flex: 1 }}
        data={data}
        order={order}
        onRowMoved={(e) => {
          order.splice(e.to, 0, order.splice(e.from, 1)[0]);
          this.forceUpdate();
        }}
        renderRow={row => <RowComponent data={row} />}
      />
    );
  }

  render() {
    return (Object.keys(data).length > 0) ? this.contentTableView() : this.placeholderView();
  }
}

const styles = StyleSheet.create({
  placeholderText: {
    fontSize: 16,
    color: '#576574',
    textAlign: 'center',
  },

  buttonStyle: {
    right: 20,
    borderRadius: 5,
  },

  buttonText: {
    fontSize: 16,
    color: '#576574',
  },

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ContentScreen;
