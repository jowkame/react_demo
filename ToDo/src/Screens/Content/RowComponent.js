import React from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableHighlight } from 'react-native';

const RowComponent = props => (
  <TouchableHighlight
    underlayColor="#eee"
    style={{
        padding: 25,
        backgroundColor: '#F8F8F8',
        borderBottomWidth: 1,
        borderColor: '#eee',
      }}

    {...props.sortHandlers}
  >
    <Text>{props.data.text}</Text>
  </TouchableHighlight>
);

RowComponent.propTypes = {
  sortHandlers: PropTypes.instanceOf(Object),
  data: PropTypes.instanceOf(Object),
};

export default RowComponent;
