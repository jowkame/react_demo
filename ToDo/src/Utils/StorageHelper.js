import Store from 'react-native-store';

const DB = {
  task: Store.model('task'),
};

class StorageHelper {
  saveTask(title, details, completion) {
    DB.task.add({
      title,
      details,
    }).then(() => {
      completion();
    });
  }

  fetchTasks(completion) {
    DB.task.find().then((resp) => {
      completion(resp || []);
    });
  }
}

export default StorageHelper;
